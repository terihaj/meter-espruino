const wifi = require('Wifi');
const http = require('http');

const STATE_LED = D15;
const WIFI_LED = D12;
const HTTP_LED = D13;

const WIFI_SSID = '';
const WIFI_OPTIONS = { password: '' };
const HOST = '';
const PORT = '';
const KEY = '';

// Interval in milliseconds
// Minimum pulse duration to read: (buffer size) * (read interval)
const SEND_INTERVAL = 60000;
const READ_INTERVAL = 10;
const BUFFER_SIZE = 8;

let buffer = [];
let count = 0;
let state = false;

function blink(pin, stateFunc, timeout) {
  digitalWrite(pin, !stateFunc());
  setTimeout(() => digitalWrite(pin, stateFunc()), timeout);
}

function getWifiState() {
  return wifi && wifi.getDetails().status === 'connected';
}

function setHttpStatus(value) {
  digitalWrite(HTTP_LED, value);
}

function setWifiStatus(value) {
  digitalWrite(WIFI_LED, value);
}

function postReading(data) {
  let body = JSON.stringify(data);
  let options = {
    host: HOST,
    port: PORT,
    path: '/readings',
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Content-Length': body.length
    }
  };

  let req = http.request(options, res => {
    console.log(res.statusCode, res.statusMessage);
    setHttpStatus(res && res.statusCode == 200);
  });

  req.on('error', err => {
    console.log('request error: ', err);
    setHttpStatus(false);
  });

  req.end(body);
}

// startup
setWifiStatus(getWifiState());

// send timer
setInterval(() => {
  blink(D2, () => true, 100);
  console.log('-');

  let data = {
    key: KEY,
    value: count,
    duration: SEND_INTERVAL / 1000
  };

  postReading(data);
  
  count = 0;
}, SEND_INTERVAL);

// read timer
setInterval(() => {
  let rawValue = analogRead(A0);
  let value = rawValue === 1;

  // echo
  digitalWrite(STATE_LED, value);

  buffer.unshift(value);
  if (buffer.length > BUFFER_SIZE) {
    buffer.pop();
  }

  if (!state && buffer.every(x => x)) {
    state = true;
    count++;
    console.log(count);
  }
  else if (state && buffer.every(x => !x)) {
    state = false;
  }
}, READ_INTERVAL);

wifi.on('disconnected', () => {
  setWifiStatus(false);
  console.log('Wifi disconnected.');
});


wifi.on('connected', () => {
  setWifiStatus(true);
  console.log('Wifi connected!');
});

function onInit() {
  if (!getWifiState()) {
    wifi.connect(WIFI_SSID, WIFI_OPTIONS, err => {
      if (err) {
        setWifiStatus(false);
        console.log(`Connection error: ${err}`);
      }
    });
  }
  else {
    setWifiStatus(true);
    console.log('Wifi still connected.');
  }
}
save();